GetBootstrap
============

Customize your Console Application using GetBootstrap.
![2 5](https://cloud.githubusercontent.com/assets/9214449/5624798/b871ce9c-9521-11e4-9d82-765afb5794eb.gif)

#### How to Use?

1. Download `GetBootstrap.dll` on [GitHub Releases](https://github.com/DITLeonel/GetBootstrap/releases)
2. `Left Cick` your console project.
3. Click `Add Reference...` menu.
4. On `Reference Manager` click `Browse...` button.
5. Locate `GetBootstrap.dll` then press `Enter`.
6. In `Program.cs` add namespace. 
```csharp
using GetBootstrap;
```
Test your `Bootstrap.Write();` command.
```csharp
Bootstrap.Write("Hello, World!");
```
