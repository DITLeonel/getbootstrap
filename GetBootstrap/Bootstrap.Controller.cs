﻿using System;
using System.Runtime.InteropServices;

namespace GetBootstrap
{
    partial class Bootstrap
    {
        const int MF_ENABLED = 0x00000000;
        const int SC_CLOSE = 0xF060;
        const int SC_MAXIMIZE = 0xF030;
        const int SC_MINIMIZE = 0xF020;

        [DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern int EnableMenu(IntPtr hMenu, int nPosition, int wFlags);

        public static void Close(bool enable)
        {
            EnableMenu(GetSystemMenu(GetConsoleWindow(), enable), SC_CLOSE, MF_ENABLED);
        }

        public static void Maximize(bool enable)
        {
            EnableMenu(GetSystemMenu(GetConsoleWindow(), enable), SC_MAXIMIZE, MF_ENABLED);
        }

        public static void Minimize(bool enable)
        {
            EnableMenu(GetSystemMenu(GetConsoleWindow(), enable), SC_MINIMIZE, MF_ENABLED);
        }
    }
}
