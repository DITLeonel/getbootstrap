﻿using System;
using System.Collections.Generic;

namespace GetBootstrap
{
    partial class Bootstrap
    {
        public static Dictionary<BootstrapType, Action<BootstrapStyle>> Customize = new Dictionary<BootstrapType, Action<BootstrapStyle>>
        {
            { BootstrapType.Default, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Gray, ConsoleColor.DarkGray, style); }},
            { BootstrapType.Success, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Green, ConsoleColor.DarkGreen, style); }},
            { BootstrapType.Info, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Cyan, ConsoleColor.DarkCyan, style); }},
            { BootstrapType.Warning, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Yellow, ConsoleColor.DarkYellow, style); }},
            { BootstrapType.Danger, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Red, ConsoleColor.DarkRed, style); }},
            { BootstrapType.Magenta, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Magenta, ConsoleColor.DarkMagenta, style); }},
            { BootstrapType.Cobalt, delegate(BootstrapStyle style) { ApplyStyle(ConsoleColor.Blue, ConsoleColor.DarkBlue, style); }}
        };

        static void ApplyStyle(ConsoleColor foregroundColor, ConsoleColor backgroundColor, BootstrapStyle style)
        {
            Console.ForegroundColor = foregroundColor;
            if (BootstrapStyle.Alert == style)
                Console.BackgroundColor = backgroundColor;
        }
    }

    public enum BootstrapStyle
    {
        Default,
        Alert
    }

    public enum BootstrapType
    {
        Default,
        Success,
        Info,
        Warning,
        Danger,
        Magenta,
        Cobalt
    }
}
