﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GetBootstrap
{
    public static partial class Bootstrap
    {
        static Random Delay = new Random();

        #region Method Write...
        public static void Write(string format, int min = 50, int max = 150)
        {
            for (int i = 0; i < format.Length; i++)
            {
                Thread.Sleep(Delay.Next(min, max > min ? max : min));
                Console.Write(format.Substring(i, 1));
            }
        }

        public static void Write(string format, BootstrapType type, BootstrapStyle style = BootstrapStyle.Default)
        {
            Customize[type](style);
            Console.Write(format);
            Console.ResetColor();
        }

        public static void Write(string format, BootstrapType type, BootstrapStyle style, int min, int max = 150)
        {
            Customize[type](style);
            Write(format, min, max);
            Console.ResetColor();
        }

        public static void Write(string format, int min, int max, params object[] args)
        {
            Write(String.Format(format, args), min, max);
        }

        public static void Write(string format, BootstrapType type, BootstrapStyle style, params object[] args)
        {
            Write(String.Format(format, args), type, style);
        }

        public static void Write(string format, BootstrapType type, BootstrapStyle style, int min, int max, params object[] args)
        {
            Write(String.Format(format, args), type, style, min, max);
        }
        #endregion

        #region Method WriteLine...
        public static void WriteLine(string format, int min = 50, int max = 150)
        {
            for (int i = 0; i < format.Length; i++)
            {
                Thread.Sleep(Delay.Next(min, max > min ? max : min));
                Console.Write(format.Substring(i, 1));
            }
            Console.WriteLine();
        }

        public static void WriteLine(string format, BootstrapType type, BootstrapStyle style = BootstrapStyle.Default)
        {
            Customize[type](style);
            Console.WriteLine(format);
            Console.ResetColor();
        }

        public static void WriteLine(string format, BootstrapType type, BootstrapStyle style, int min, int max = 150)
        {
            Customize[type](style);
            WriteLine(format, min, max);
            Console.ResetColor();
        }

        public static void WriteLine(string format, int min, int max, params object[] args)
        {
            WriteLine(String.Format(format, args), min, max);
        }

        public static void WriteLine(string format, BootstrapType type, BootstrapStyle style, params object[] args)
        {
            WriteLine(String.Format(format, args), type, style);
        }

        public static void WriteLine(string format, BootstrapType type, BootstrapStyle style, int min, int max, params object[] args)
        {
            WriteLine(String.Format(format, args), type, style, min, max);
        }
        #endregion

        public static void Popup(string format, string caption, params object[] args)
        {
            MessageBox.Show(String.Format(format, args), caption);
        }
    }
}
